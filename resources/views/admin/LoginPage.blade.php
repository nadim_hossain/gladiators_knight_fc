<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WELCOME TO LOGIN PAGE!!</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/LoginPage.css') }}">
</head>
<body>
    <div class="login-box">
        <h2>Login</h2>

        @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="alert alert-danger">
                    {{ session()->get('error') }}
                    </div>
                @endif

        <form action="{{route('login')}}" method="POST">
            {{@csrf_field()}}
            <div class="user-box">
                <input type="text" name="uname" required="" value="{{old('username')}}">
                <label>Username</label>
            </div>
            <div class="user-box">
                <input type="password" name="password" required="" value="{{old('password')}}">
                <label>Password</label>
            </div>
                <button style="background-color: #141e30;">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    LOGIN
                </button>
        </form>
    </div>
</body>
</html>