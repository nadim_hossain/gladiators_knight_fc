<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ URL::asset('css/SideManu.css') }}">
    <title>WELCOME TO ADMIN PANAL</title>
</head>
    <body>
        @extends('layouts.SideManu')
            @section('content')

                <!-- CONTENT -->
                <section id="content">
                    <!-- NAVBAR -->
                    <nav>
                        <i class='bx bx-menu' ></i>
                        <a href="#" class="nav-link">Categories</a>
                        <form action="#">
                            <div class="form-input">
                                <input type="search" placeholder="Search...">
                                <button type="submit" class="search-btn"><i class='bx bx-search' ></i></button>
                            </div>
                        </form>
                        <input type="checkbox" id="switch-mode" hidden>
                        <label for="switch-mode" class="switch-mode"></label>
                        <a href="#" class="notification">
                            <i class='bx bxs-bell' ></i>
                            <span class="num">8</span>
                        </a>
                        <a href="#" class="profile">
                            <img src="{{URL::asset('images/people.png')}}">
                        </a>
                    </nav>
                    <!-- NAVBAR -->

                    <!-- MAIN -->
                    <main>
                        <div class="head-title">
                            <div class="left">
                                <h1>Dashboard</h1>
                                <ul class="breadcrumb">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li><i class='bx bx-chevron-right' ></i></li>
                                    <li>
                                        <a class="active" href="#">Home</a>
                                    </li>
                                </ul>
                            </div>
                            <a href="#" class="btn-download">
                                <i class='bx bxs-cloud-download' ></i>
                                <span class="text">Download PDF</span>
                            </a>
                        </div>

                        <ul class="box-info">
                            <li>
                                <i class='bx bxs-calendar-check' ></i>
                                <span class="text">
                                    <h3>1020</h3>
                                    <p>New Order</p>
                                </span>
                            </li>
                            <li>
                                <i class='bx bxs-group' ></i>
                                <span class="text">
                                    <h3>2834</h3>
                                    <p>Visitors</p>
                                </span>
                            </li>
                            <li>
                                <i class='bx bxs-dollar-circle' ></i>
                                <span class="text">
                                    <h3>$2543</h3>
                                    <p>Total Sales</p>
                                </span>
                            </li>
                        </ul>


                        <div class="table-data">
                            <div class="order">
                                <div class="head">
                                    <h3>Recent Orders</h3>
                                    <i class='bx bx-search' ></i>
                                    <i class='bx bx-filter' ></i>
                                </div>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Date Order</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src="img/people.png">
                                                <p>John Doe</p>
                                            </td>
                                            <td>01-10-2021</td>
                                            <td><span class="status completed">Completed</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="img/people.png">
                                                <p>John Doe</p>
                                            </td>
                                            <td>01-10-2021</td>
                                            <td><span class="status pending">Pending</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="img/people.png">
                                                <p>John Doe</p>
                                            </td>
                                            <td>01-10-2021</td>
                                            <td><span class="status process">Process</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="img/people.png">
                                                <p>John Doe</p>
                                            </td>
                                            <td>01-10-2021</td>
                                            <td><span class="status pending">Pending</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="img/people.png">
                                                <p>John Doe</p>
                                            </td>
                                            <td>01-10-2021</td>
                                            <td><span class="status completed">Completed</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </main>
                    <!-- MAIN -->
                </section>
                <!-- CONTENT -->
            @endsection
            <script type="text/javascript">
                var allSideMenu = document.querySelectorAll('#sidebar .side-menu.top li a');

        allSideMenu.forEach(item=> {
            var li = item.parentElement;

            item.addEventListener('click', function () {
                allSideMenu.forEach(i=> {
                    i.parentElement.classList.remove('active');
                })
                li.classList.add('active');
            })
        });




        // TOGGLE SIDEBAR
        var menuBar = document.querySelector('#content nav .bx.bx-menu');
        var sidebar = document.getElementById('sidebar');

        menuBar.addEventListener('click', function () {
            sidebar.classList.toggle('hide');
        })







        var searchButton = document.querySelector('#content nav form .form-input button');
        var searchButtonIcon = document.querySelector('#content nav form .form-input button .bx');
        var searchForm = document.querySelector('#content nav form');

        searchButton.addEventListener('click', function (e) {
            if(window.innerWidth < 576) {
                e.preventDefault();
                searchForm.classList.toggle('show');
                if(searchForm.classList.contains('show')) {
                    searchButtonIcon.classList.replace('bx-search', 'bx-x');
                } else {
                    searchButtonIcon.classList.replace('bx-x', 'bx-search');
                }
            }
        })





        if(window.innerWidth < 768) {
            sidebar.classList.add('hide');
        } else if(window.innerWidth > 576) {
            searchButtonIcon.classList.replace('bx-x', 'bx-search');
            searchForm.classList.remove('show');
        }


        window.addEventListener('resize', function () {
            if(this.innerWidth > 576) {
                searchButtonIcon.classList.replace('bx-x', 'bx-search');
                searchForm.classList.remove('show');
            }
        })



        var switchMode = document.getElementById('switch-mode');

        switchMode.addEventListener('change', function () {
            if(this.checked) {
                document.body.classList.add('dark');
            } else {
                document.body.classList.remove('dark');
            }
        })
    </script>
    </body>
</html>