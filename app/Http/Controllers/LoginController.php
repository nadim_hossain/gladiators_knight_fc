<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    public function login(){
        return view('admin.LoginPage');
    }

    public function loginsubmit(Request $request){
        $validate = Validator::make($request->all(), [
            "uname"=>"required|min:5|max:10",
            "password"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            ]);

        if($validate->fails()){
            $error = $validate->getMessageBag();
            return redirect()->route('login')
                 ->with('error', $error);
        }
        else
        $admin = Admin::where('username', $request->uname)
            ->where('password' ,$request->password)
            ->first();

            // $user = Buyer::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            // $seller = Seller::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            // $deliveris = Deliveri::where('username', $request->username)
            // ->where('password' ,($request->password))
            // ->first();

            if($admin){
                session()->put('admin',$request->uname);
                 if ($request->remember) {
                     setcookie('remember',$request->uname, time()+36000);
                     Cookie::queue('name',$admin->uname,time()+60);
                 }
                 return redirect()->route('dashboard');
            }
            else{
                $error = "Username and Password Incorect. Try Again";
                 return redirect()->route('login')
                 ->with('error', $error);
            }

            // elseif($user){
            //     session()->put('user',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('payment');
            // }

            // elseif($seller){
            //     session()->put('seller',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('change_password');
            // }

            // elseif($deliveris){
            //     session()->put('deliveris',$request->username);
            //     if ($request->remember) {
            //         setcookie('remember',$request->username, time()+36000);
            //         Cookie::queue('name',$admin->username,time()+60);
            //     }
            //     return redirect()->route('accounts');
            // }

            // else{
            //     $error = "Username and Password Incorect. Try Again";
            //     return redirect()->route('login')
            //     ->with('error', $error);
            // }
    }
    public function logout(){
        session()->forget('admin');
        return redirect()->route('login');
    }

}
